package com.falendysh.testgallery.viewmodel

import android.app.Application
import android.text.TextUtils
import androidx.lifecycle.*
import com.falendysh.testgallery.model.ImageRecord
import com.falendysh.testgallery.repository.ImageRecordRepository
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

class ImageRecordViewModel(application: Application): AndroidViewModel(application) {

    private val imagesLiveData = MutableLiveData<ArrayList<ImageRecord>>()
    private val database = ImageRecordRepository.getDataBase(application)
    private val images = ArrayList<ImageRecord>()
    private var search = ""

    fun getAllImages(search: String) {
        this.search = "%$search%"
        viewModelScope.launch { getImages(search) }
    }

    private suspend fun getImages(search: String) = withContext(Dispatchers.Main) {
        if (TextUtils.isEmpty(search)) getImages() else searchImages()
        imagesLiveData.postValue(images)
    }

    private suspend fun getImages() = withContext(Dispatchers.IO) {
        images.clear()
        images.addAll(database.imageRecordDAO().getImages())
    }

    private suspend fun searchImages() = withContext(Dispatchers.IO) {
        images.clear()
        images.addAll(database.imageRecordDAO().searchImages(search))
    }

    fun addImageRecord(imagePath: String, shortDescription: String, longDescription: String, latitude: Double, longitude: Double) {
        viewModelScope.launch {
            withContext(Dispatchers.IO) {
                val imageRecord = ImageRecord(imagePath, shortDescription, longDescription, latitude, longitude)
                database.imageRecordDAO().insertImageRecord(imageRecord)
            }
            getImages(search)
        }
    }

    fun updateImageRecord(imageRecord: ImageRecord) {
        viewModelScope.launch {
            withContext(Dispatchers.IO) {
                database.imageRecordDAO().updateImageRecord(imageRecord)
            }
            getImages(search)
        }
    }

    fun deleteImageRecord(imageRecord: ImageRecord) {
        viewModelScope.launch {
            withContext(Dispatchers.IO) {
                database.imageRecordDAO().deleteImageRecord(imageRecord)
            }
            getImages(search)
        }
    }

    fun getImagesLiveData(): LiveData<ArrayList<ImageRecord>> {
        return imagesLiveData
    }

}