package com.falendysh.testgallery

import android.Manifest
import android.app.Activity
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.location.Location
import android.os.Build
import android.os.Bundle
import android.provider.MediaStore
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.Menu
import android.view.MenuInflater
import android.view.MenuItem
import androidx.activity.viewModels
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.observe
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import com.falendysh.testgallery.model.ImageRecord
import com.falendysh.testgallery.ui.ImageRecordGridAdapter
import com.falendysh.testgallery.ui.ImageRecordListAdapter
import com.falendysh.testgallery.utils.ImageUtil
import com.falendysh.testgallery.viewmodel.ImageRecordViewModel
import com.google.android.gms.location.LocationServices
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.image_record_edit_layout.view.*
import java.io.File

class MainActivity : AppCompatActivity() {

    private val viewModel by viewModels<ImageRecordViewModel>()
    private var listAdapter: ImageRecordListAdapter? = null
    private var gridAdapter: ImageRecordGridAdapter? = null
    private var listLayoutManager: LinearLayoutManager? = null
    private var gridLayoutManager: GridLayoutManager? = null

    private val CAMERA_REQUEST_CODE = 12345
    private val LOCATION_PERMISSIONS_REQUEST_CODE = 7777

    private val LOCATION_PERMISSIONS = arrayOf(
        Manifest.permission.ACCESS_COARSE_LOCATION,
        Manifest.permission.ACCESS_FINE_LOCATION
    )

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (checkSelfPermission(Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED &&
                checkSelfPermission(Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED
            ) requestPermissions(LOCATION_PERMISSIONS, LOCATION_PERMISSIONS_REQUEST_CODE)
            else startLocationClient()
        } else
            startLocationClient()

        listAdapter = ImageRecordListAdapter(this)
        gridAdapter = ImageRecordGridAdapter(this)

        listAdapter?.getSelectedImageRecordLiveData()?.observe(this) {
            displayImageRecord(it)
        }

        gridAdapter?.getSelectedImageRecordLiveData()?.observe(this) {
            displayImageRecord(it)
        }

        listLayoutManager = LinearLayoutManager(this)
        gridLayoutManager = GridLayoutManager(this, 3)

        viewModel.getImagesLiveData().observe(this, {
            displayImageRecords(it)
        })
        viewModel.getAllImages("")

        searchEditText.addTextChangedListener((object : TextWatcher {

            override fun afterTextChanged(s: Editable) {}

            override fun beforeTextChanged(s: CharSequence, start: Int,
                                           count: Int, after: Int) {}

            override fun onTextChanged(s: CharSequence, start: Int,
                                       before: Int, count: Int) {
                viewModel.getAllImages(s.toString())
            }
        }))

        getPhotoButton.setOnClickListener {getPhoto()}
    }

    private fun displayImageRecord(imageRecord: ImageRecord) {
        val dialogBuilder = AlertDialog.Builder(this)
        val dialogView = LayoutInflater.from(this).inflate(R.layout.image_record_edit_layout, null, false)
        dialogBuilder.setView(dialogView)
        val dialog = dialogBuilder.create()

        val imageFile = File(imageRecord.imagePath)
        Picasso.get()
            .load(imageFile)
            .into(dialogView.dialogImageView)

        dialogView.locationTextView.text = "Lat: ${imageRecord.latitude}, Lon: ${imageRecord.longitude}"

        dialogView.shortEditText.setText(imageRecord.shortDescription)
        dialogView.longEditText.setText(imageRecord.longDescription)
        dialogView.saveButton.setOnClickListener {
            imageRecord.shortDescription = dialogView.shortEditText.text.toString()
            imageRecord.longDescription = dialogView.longEditText.text.toString()
            viewModel.updateImageRecord(imageRecord)
            dialog.dismiss()
        }
        dialogView.deleteButton.setOnClickListener {
            viewModel.deleteImageRecord(imageRecord)
            dialog.dismiss()
        }

        dialog.show()
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        if (PackageManager.PERMISSION_DENIED in grantResults) return
        startLocationClient()
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
    }

    private fun startLocationClient() {
        val fusedLocationClient = LocationServices.getFusedLocationProviderClient(this)
        fusedLocationClient.lastLocation.addOnSuccessListener {
            location = it
        }
    }

    private fun getPhoto() {
        val intent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
        startActivityForResult(intent, CAMERA_REQUEST_CODE)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (requestCode == CAMERA_REQUEST_CODE && resultCode == Activity.RESULT_OK) {
            val bitmap: Bitmap = data?.extras?.get("data") as Bitmap
            val filename = ImageUtil(this).saveBitmapToFile(bitmap)
            viewModel.addImageRecord(filename, "short", "long", getLatitude(), getLongitude())
            return
        }

        super.onActivityResult(requestCode, resultCode, data)
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        val menuInflater = MenuInflater(this)
        menuInflater.inflate(R.menu.menu, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.listMode -> {
                asList = true
                displayAsList()
                return true

            }
            R.id.gridMode -> {
                asList = false
                displayAsGrid()
                return true
            }
        }
        return super.onOptionsItemSelected(item)
    }

    private fun displayImageRecords(imageRecords: ArrayList<ImageRecord>) {
        if (asList) displayAsList() else displayAsGrid()
        listAdapter?.setData(imageRecords)
        gridAdapter?.setData(imageRecords)
    }

    private fun displayAsList() {
        recyclerView.adapter = listAdapter
        recyclerView.layoutManager = listLayoutManager
    }

    private fun displayAsGrid() {
        recyclerView.adapter = gridAdapter
        recyclerView.layoutManager = gridLayoutManager
    }

    companion object {
        var asList: Boolean = true
        var location: Location? = null
        fun getLatitude(): Double {
            return if (location == null) 0.0 else location?.latitude!!
        }
        fun getLongitude(): Double {
            return if (location == null) 0.0 else location?.longitude!!
        }

    }
}
