package com.falendysh.testgallery.utils

import android.content.Context
import android.graphics.Bitmap
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import java.io.File
import java.io.FileOutputStream
import java.util.*

class ImageUtil(private var context: Context) {
    fun saveBitmapToFile(bitmap: Bitmap): String {
        val shortFileName = Date().time.toString()+".jpg"
        val dir = context.filesDir
        val subDir = File(dir, "imagesDB")
        if (!subDir.exists()) subDir.mkdir()
        val imageFile = File(subDir, shortFileName)
        CoroutineScope(Dispatchers.Main).launch {
            withContext(Dispatchers.IO) {
                val fileOutputStream = FileOutputStream(imageFile)
                bitmap.compress(Bitmap.CompressFormat.JPEG, 100, fileOutputStream)
                fileOutputStream.flush()
                fileOutputStream.close()
            }
        }
        return imageFile.absolutePath
    }
}