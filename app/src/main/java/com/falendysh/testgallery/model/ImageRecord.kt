package com.falendysh.testgallery.model

import android.net.Uri
import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.Ignore
import androidx.room.PrimaryKey

@Entity
data class ImageRecord (
    @PrimaryKey(autoGenerate = true)
    val id: Long,
    @ColumnInfo(name = "image_path")
    val imagePath: String,
    @ColumnInfo(name = "short_description")
    var shortDescription: String,
    @ColumnInfo(name = "long_description")
    var longDescription: String,
    @ColumnInfo(name = "latitude")
    val latitude: Double,
    @ColumnInfo(name = "longitude")
    val longitude: Double
)
{
    @Ignore
    constructor(imagePath: String,
                shortDescription: String,
                longDescription: String,
                latitude: Double,
                longitude: Double): this(0, imagePath, shortDescription, longDescription, latitude, longitude )
}