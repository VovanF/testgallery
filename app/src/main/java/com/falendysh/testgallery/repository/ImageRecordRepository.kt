package com.falendysh.testgallery.repository

import android.app.Application
import androidx.room.Room

class ImageRecordRepository {
    companion object {
        private var dataBase: ImageRecordDatabase? = null

        @Synchronized
        fun getDataBase(application: Application): ImageRecordDatabase {
            if (dataBase == null) {
                dataBase = Room.databaseBuilder(application.applicationContext,
                    ImageRecordDatabase::class.java,
                    "ImageDataBase").build()
            }
            return dataBase!!
        }

    }
}