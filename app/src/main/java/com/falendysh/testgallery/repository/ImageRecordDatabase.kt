package com.falendysh.testgallery.repository

import androidx.room.Database
import androidx.room.RoomDatabase
import com.falendysh.testgallery.model.ImageRecord

@Database(entities = [ImageRecord::class], version = 1)
abstract class ImageRecordDatabase: RoomDatabase() {
    abstract fun imageRecordDAO(): ImageRecordDAO
}