package com.falendysh.testgallery.repository

import androidx.room.*
import com.falendysh.testgallery.model.ImageRecord

@Dao
interface ImageRecordDAO {
    @Query("SELECT * FROM imageRecord")
    suspend fun getImages(): List<ImageRecord>

    @Query("SELECT * FROM imageRecord WHERE short_description LIKE :search OR long_description LIKE :search")
    suspend fun searchImages(search: String): List<ImageRecord>

    @Update
    suspend fun updateImageRecord(imageRecord: ImageRecord)

    @Insert
    suspend fun insertImageRecord(imageRecord: ImageRecord)

    @Delete
    suspend fun deleteImageRecord(imageRecord: ImageRecord)
}