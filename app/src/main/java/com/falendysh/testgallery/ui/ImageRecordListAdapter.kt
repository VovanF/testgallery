package com.falendysh.testgallery.ui

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import com.falendysh.testgallery.R

class ImageRecordListAdapter(context: Context) : ImageRecordAbstractAdapter(context) {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ImageRecordViewHolder {
        val view = LayoutInflater.from(context).inflate(R.layout.image_record_list_item_layout, parent, false)
        return ImageRecordViewHolder(view)
    }
}