package com.falendysh.testgallery.ui

import android.content.Context
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.recyclerview.widget.RecyclerView
import com.falendysh.testgallery.R
import com.falendysh.testgallery.model.ImageRecord
import com.squareup.picasso.Picasso
import java.io.File

abstract class ImageRecordAbstractAdapter(val context: Context): RecyclerView.Adapter<ImageRecordAbstractAdapter.ImageRecordViewHolder>() {

    private val imageData = ArrayList<ImageRecord>()
    private val selectedImageRecordLiveData = MutableLiveData<ImageRecord>()

    fun setData(data: ArrayList<ImageRecord>) {
        imageData.clear()
        imageData.addAll(data)
        notifyDataSetChanged()
    }

    override fun getItemCount(): Int {
        return imageData.size
    }

    fun getSelectedImageRecordLiveData(): LiveData<ImageRecord> {
        return selectedImageRecordLiveData
    }

    override fun onBindViewHolder(holder: ImageRecordViewHolder, position: Int) {
        val imageFile = File(imageData[position].imagePath)
        Picasso.get()
            .load(imageFile)
            .into(holder.imageView)
        holder.textView.text = imageData[position].shortDescription
        holder.itemView.setOnClickListener {
            selectedImageRecordLiveData.postValue(imageData[position])
        }
    }

    class ImageRecordViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val imageView: ImageView = itemView.findViewById(R.id.imageView)
        val textView: TextView = itemView.findViewById(R.id.textView)
    }
}